package arrays;

public class Task1 {
	public static void main(String args[]) {
		System.out.println(getTxtIndex(new String[]{"task1.java", "task2.java", "task3.java", "task4.txt"}) == 3);
		System.out.println(getTxtIndex(new String[0]) == -1);
		System.out.println(getTxtIndex(new String[]{"README.TXT", "a.mp4", "b.mp4", "c.mp4"}) == 0);
		System.out.println(getTxtIndex(new String[]{"steam.exe", "origin.exe", "psplus.exe"}) == -1);
		System.out.println(getTxtIndex(new String[]{"1st", "2nd", "3rd.Txt", "4th"}) == 2);
		System.out.println(getTxtIndex(new String[]{"Tinker.txt.png", "Tailor.txt.bpf", "Solder.txt.jpg", "Spy"}) == -1);
		System.out.println("Finish");
	}

	static int getTxtIndex(String[] filenames) {
		return -1;
	}
}
