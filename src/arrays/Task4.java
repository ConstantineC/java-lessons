package arrays;

import java.util.Arrays;

public class Task4 {
	public static void main(String args[]) {
		int case1[] = new int[] {10, -3, -19, -6, -18};
		int answer1[] = new int[] {-18, -6, -19, -3, 10};
		System.out.println(Arrays.equals(reversed(case1), answer1));

		int case2[] = new int[] {};
		int answer2[] = new int[] {};
		System.out.println(Arrays.equals(reversed(case2), answer2));

		int case3[] = new int[] {0, -16, 9, -17, -12, 14, 12, 2, -4, 6};
		int answer3[] = new int[] {6, -4, 2, 12, 14, -12, -17, 9, -16, 0};
		System.out.println(Arrays.equals(reversed(case3), answer3));

		int case4[] = new int[] {-3};
		int answer4[] = new int[] {-3};
		System.out.println(Arrays.equals(reversed(case4), answer4));

		int case5[] = new int[] {3, 4, 14, -17, 20, -17, -15, -18, 17, 2, 12, -14};
		int answer5[] = new int[] {-14, 12, 2, 17, -18, -15, -17, 20, -17, 14, 4, 3};
		System.out.println(Arrays.equals(reversed(case5), answer5));


		System.out.println("Finish");
	}

	static int[] reversed(int data[]) {
		return data;
	}
}
