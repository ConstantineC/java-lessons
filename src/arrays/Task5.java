package arrays;

public class Task5 {
    public static void main(String args[]) {
        String case1[] = new String[] {"Sunray", "Key Lime", "Kenyan copper", "Rust", "Willpower orange"};
        String answer1 = "Sunray;Key Lime;Kenyan copper;Rust;Willpower orange";
        System.out.println(join(case1).equals(answer1));

        String case2[] = new String[] {"B'dazzled blue", "Azure (web color)", "Trolley Grey", "Beige", "Electric lavender", "Pear", "Mauve taupe", "Ao (English)", "Dark scarlet", "Telemagenta"};
        String answer2 = "B'dazzled blue;Azure (web color);Trolley Grey;Beige;Electric lavender;Pear;Mauve taupe;Ao (English);Dark scarlet;Telemagenta";
        System.out.println(join(case2).equals(answer2));

        String case3[] = new String[] {"Spiro Disco Ball"};
        String answer3 = "Spiro Disco Ball";
        System.out.println(join(case3).equals(answer3));

        String case4[] = new String[] {};
        String answer4 = "";
        System.out.println(join(case4).equals(answer4));

        String case5[] = new String[] {"Skobeloff", "Space cadet", "Manatee", "Dandelion"};
        String answer5 = "Skobeloff;Space cadet;Manatee;Dandelion";
        System.out.println(join(case5).equals(answer5));

        String case6[] = new String[] {"Fuchsia rose", "Light pastel purple", "Salmon pink", "Viridian", "Peach puff", "Giants orange", "Sapphire blue", "Yellow (RYB)", "Rubine red", "Forest green (traditional)", "Chocolate (traditional)", "Bright turquoise", "Malachite", "Japanese violet", "Steel pink", "Light pink", "Yellow", "Pastel green", "Giants orange", "Red-orange"};
        String answer6 = "Fuchsia rose;Light pastel purple;Salmon pink;Viridian;Peach puff;Giants orange;Sapphire blue;Yellow (RYB);Rubine red;Forest green (traditional);Chocolate (traditional);Bright turquoise;Malachite;Japanese violet;Steel pink;Light pink;Yellow;Pastel green;Giants orange;Red-orange";
        System.out.println(join(case6).equals(answer6));


        System.out.println("Finish");
    }

    static String join(String []colors) {
        return "";
    }
}
