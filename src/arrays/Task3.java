package arrays;

import static java.lang.Math.abs;

public class Task3 {

    public static final double EPSILON = 0.0001;

    public static void main(String args[]) {
        double case1[] = new double[] {-11.65, 15.93, -0.49, 15.68, -8.7};
        double delta = getMaxMin(case1) - 4.280000;
        System.out.println(abs(delta) < EPSILON);

        double case2[] = new double[] {-2.6, 0.49, -12.62, 15.89, -12.09, -7.79, -3.33, 19.21, 13.79, 16.53};
        delta = getMaxMin(case2) - 6.590000;
        System.out.println(abs(delta) < EPSILON);

        double case3[] = new double[] {1.39};
        delta = getMaxMin(case3) - 2.780000;
        System.out.println(abs(delta) < EPSILON);

        double case4[] = new double[] {10.0, 18.88, 12.56, -19.5};
        delta = getMaxMin(case4) + 0.620000;
        System.out.println(abs(delta) < EPSILON);

        double case5[] = new double[] {-7.95, -8.62, -7.46, -18.93, -5.79, -8.38, 2.09, 16.19, 7.86, 13.16, -8.92, -11.01, 19.35, -17.42, 4.48, 1.03, -7.07, -6.78, 19.13, 15.11};
        delta = getMaxMin(case5) - 0.420000;
        System.out.println(abs(delta) < EPSILON);

        double case6[] = new double[] {-1., -1., -1., -1., -1.};
        delta = getMaxMin(case6) + 2.;
        System.out.println(abs(delta) < EPSILON);

        double case7[] = new double[] {2., -2., 1., -2., 2.};
        System.out.println(abs(getMaxMin(case7)) < EPSILON);

        System.out.println("Finish");
    }

    static double getMaxMin(double []values) {
        return 0;
    }
}
