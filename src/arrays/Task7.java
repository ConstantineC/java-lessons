package arrays;

import java.util.Arrays;

public class Task7 {
    public static void main(String args[]) {
        int case1[][] = {{6, -3, -2},
                {12, 3, 18},
                {6, 18, 11}};
        int answer1[][] =  {{6, 12, 6},
                {-3, 3, 18},
                {-2, 18, 11}};
        System.out.println(matrixEquals(transponeMatrix(case1), answer1));

        int case2[][] = {{2, 7, -1, 18},
                {6, 15, -1, 3},
                {-8, 3, 0, 16}};
        int answer2[][] = {{2, 6, -8},
                {7, 15, 3},
                {-1, -1, 0},
                {18, 3, 16}};
        System.out.println(matrixEquals(transponeMatrix(case2), answer2));

        int case3[][] = {{12, -10},
                {3, 6}};
        int answer3[][] = {{12, 3},
                {-10, 6}};
        System.out.println(matrixEquals(transponeMatrix(case3), answer3));

        int case4[][] = {{5, 15, -5, 7, -8}};
        int answer4[][] = {{5},
                {15},
                {-5},
                {7},
                {-8}};
        System.out.println(matrixEquals(transponeMatrix(case4), answer4));

        int case5[][] = {{17},
                {18},
                {7},
                {6}};
        int answer5[][] = {{17, 18, 7, 6}};
        System.out.println(matrixEquals(transponeMatrix(case5), answer5));

        int case6[][] = {{20, 16},
                {-4, -4},
                {-2, 15}};
        int answer6[][] = {{20, -4, -2},
                {16, -4, 15}};
        System.out.println(matrixEquals(transponeMatrix(case6), answer6));

        int case7[][] = {{-3}};
        int answer7[][] = {{-3}};
        System.out.println(matrixEquals(transponeMatrix(case7), answer7));


        System.out.println("Finish");
    }

    static boolean matrixEquals(int m1[][], int m2[][]) {
        if(m1.length != m2.length) return false;
        for(int i=0; i<m1.length; i++) {
            if(!Arrays.equals(m1[i], m2[i])) {
                return false;
            }
        }

        return true;
    }

    static int[][] transponeMatrix(int m[][]) {
        return new int[][] {{m[0][0]}};
    }
}
