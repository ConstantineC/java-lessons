package basic;

public class Task3 {
	public static void main() {
		System.out.println(sumDigits(717) == 15);
		System.out.println(sumDigits(115) == 7);
		System.out.println(sumDigits(918) == 18);
		System.out.println(sumDigits(5) == 5);
		System.out.println(sumDigits(52) == 7);
		System.out.println(sumDigits(0) == 0);
		System.out.println(sumDigits(103) == 4);
		System.out.println(sumDigits(188) == 17);
		System.out.println("Finish");
	}

	static int sumDigits(int x) {
		assert(x<1000);
		return 0;
	}
}
